import React, { Component } from 'react';
import Button from "../components/Button.jsx";
import InputField from "../components/InputField.jsx";
import FeedbackMsg from "../components/FeedbackMsg.jsx";

class ModalAddUser extends Component {

	constructor(props) {
		super(props);
		this.state = {
			/**
			 * Mensaje de feedback
			 */
			feedbackMsg: '',

			/**
			 * Tipo de feedback (error, succes)
			 */
			feedbackType: '',

			/**
			 * Nombre usuario
			 */
			userName: '',

			/**
			 * Descripción usuario
			 */
			userDescription: '',

			/**
			 * Foto usuario
			 */
			userPhoto: '',

			/**
			 * Error al ingresar nombre (nombre vacío)
			 */
			nameError: false,

			/**
			 * Error al ingresar url foto (url foto vacío)
			 */
			photoError: false,
			
			/**
			 * Error al ingresar descripción (descripción vacía)
			 */
			descError: false,

			/**
			 * Bind this para cerrar modal
			*/
			closeModal: props.closeModal
		}

		/**
		 * Bind this para actualización de value en input
		*/
		this.handleUserName 		= this.handleUserName.bind(this);
		
		/**
		 * Bind this para actualización de value en input
		*/
		this.handleUserDescription 	= this.handleUserDescription.bind(this);
		
		/**
		 * Bind this para actualización de value en input
		*/
		this.handleUserPhoto 		= this.handleUserPhoto.bind(this);
		
		/**
		 * Bind this para actualización de value en input
		*/
		this.saveUser 				= this.saveUser.bind(this);
				
		
		/**
		 * Bind this para actualización de value en input
		*/
		this.clearForm				= this.clearForm.bind(this);
		
	}

  	/**
	 * Handle nombre completo
	 * @param {object} e Event
	 */
  	handleUserName(e) {
		this.setState({userName: e.target.value});
  	}
  	/**
	 * Handle descripción
	 * @param {object} e Event
	 */
  	handleUserDescription(e) {
		this.setState({userDescription: e.target.value});
  	}
  	/**
	 * Handle foto
	 * @param {object} e Event
	 */
  	handleUserPhoto(e) {
	  this.setState({userPhoto: e.target.value});
  	}

  	/**
	 * Vaciar el formulario
	 */
	clearForm() {
		this.setState({
			isLoading: false,
			userName: '',
			userDescription: '',
			userPhoto: ''
	  	});
	  }
	  
  	/**
	   * Guardar nuevo Usuario en Base de Datos
	   */
	saveUser() {
		/**
		 * Activar loader cambiato state isLoading
		 */
		this.setState({
			isLoading: true,
		});

		/**
		 * Check error en los campos
		 */
		let error = false;

		/**
		 * Si el nombre está vacío, cambiar state nameError a true y gatillar mensaje de error
		 */
		if (this.state.userName === '' ) {
			this.setState({
				nameError: true
			});
			error = true;
		}
		/**
		 * Si la descripción está vacía, cambiar state descError a true y gatillar mensaje de error
		 */
		if (this.state.userDescription === '' ) {
			this.setState({
				descError: true
			});
			error = true;
		}
		/**
		 * Si la foto está vacía, cambiar state photoError a true y gatillar mensaje de error
		 */
		if (this.state.userPhoto === '' ) {
			this.setState({
				photoError: true
			});
			error = true;
		}		
			

		/**
		 * No existen errores en los campos ingresados
		 */
		if (!error) {
			/**
			 * Post con datos de usuario a la base de datos mediante API
			 */
			fetch('/api/users', {
				method: 'POST',
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
				},
				body: JSON.stringify({
					name: this.state.userName,
					description: this.state.userDescription,
					photo: this.state.userPhoto,				
				})
			})
			.then((response) => response.json())
			.then((responseJson) => {
				const newUserId 	= responseJson.id;
				const newUserName 	= responseJson.name;
				this.setState({
					isLoading: false,
					feedbackMsg: 'Usuario ' + newUserName +  ' agregado con éxito  (ID: ' + newUserId + ')',
					feedbackType: 'success',
				});
				this.clearForm();
			})
			.catch((error) => {
				error = true;
				console.error(error);
			});
		}
		
		/**
		 * Desactivar Loader
		 */
		this.setState({
			isLoading: false,
		});

		return error;
	}	  	

  render() {

	/**
	 * Clase active para modal
	 */
    const active = this.props.isVisible ? 'active' : '';

    return (
    <div className={`modal__adduser modal__window ${active}`}>
      	<div className="modal__dialog">
			<header className="modal__header">
			<h4 className="modal__title">Agregar nuevo contacto</h4>
			</header>
			<section className="modal__content">
				<form action="" className="form__adduser">
					<div className="form__section">
						<label htmlFor="add_user-photo">URL Imagen del Perfil <span className="required__star text--color-orange">*</span></label>
						<InputField
							id="add_user-photo"
							type="text"
							name="add_user-photo"
							inputVal={this.state.userPhoto}
							controlFunc={this.handleUserPhoto} />
					</div>
					<div className="form__section">
						<label htmlFor="add_user-name">Nombre <span className="required__star text--color-orange">*</span></label>
						<InputField
							id="add_user-name"
							type="text"
							name="add_user-name"
							inputVal={this.state.userName}
							controlFunc={this.handleUserName} />
					</div>
					<div className="form__section">
						<label htmlFor="add_user-description">Descripción <span className="required__star text--color-orange">*</span></label>
						<InputField
							id="add_user-photo"
							type="textarea"
							name="add_user-description"
							inputVal={this.state.userDescription}
							controlFunc={this.handleUserDescription} />
					</div>
				</form>
			</section>
			<section className="modal__feedback">
				<FeedbackMsg 
					msg={this.state.feedbackMsg}
					type={this.state.feedbackType}
				/>
			</section>
			<footer className="modal__controls">
				<Button
					text="Cerrar"
					buttonClick={this.state.closeModal}
					extraClass="text--color-white"/>
				<Button
					text="Guardar"
					buttonClick={this.saveUser}
					extraClass="text--color-white"/>
			</footer>
		</div>
	</div>
    );
  }
}

export default ModalAddUser;
