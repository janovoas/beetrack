import React from "react";
import '../styles/css/Button.css';

function Button(props) {
  /**
   * Texto del botón
   */
  const text = props.text;
  
  /**
   * Ícono del botón (Font Awesome)
   */
  const icon = props.iconClass === "" ? "" : (<i className={props.iconClass} />);
  
  /**
   * Función onClick
   */
  const buttonClick = props.buttonClick;  

  return (
    <button className={`button ${props.extraClass}`}
    onClick={buttonClick}
  >{icon}{text}</button>
  );
}

export default Button;
