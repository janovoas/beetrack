/**
 * 
 * @param {string} query String de búsqueda
 */
function search(currentPage, query) {
    console.log(this);
    return fetch(`api/users?_page=${currentPage}&_limit=4&q=${query}`)
        .then(response => {
            return response.json();
        })
        .then(data => {
            if (data.length > 0) {
                this.setState({
                    users: data,
                    currentPage: currentPage
                });
            }
        })
        .catch(error => console.warn(error));
}

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
        return response;
    }
    const error = new Error(`HTTP Error ${response.statusText}`);
    error.status = response.statusText;
    error.response = response;
    console.log(error);
    throw error;
}

function parseJSON(response) {
  return response.json();
}

export {search};