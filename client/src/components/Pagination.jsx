import React from 'react';
import Button from './Button';

function Pagination(props) {
    /**
     * Función para paginar desde API
     */
    const paginationFunc = props.paginationFunc;
    
    return (<div className="pagination">
                <Button
                    text="Página Anterior"
                    extraClass="prev-page page__nav text--color-gray nobg nopad"
                    buttonClick={paginationFunc}
                    iconClass="fas fa-arrow-circle-left"/>
                <Button
                    text="Siguiente Página"
                    extraClass="next-page page__nav text--color-gray nobg nopad icon-right"
                    buttonClick={paginationFunc} 
                    iconClass="fas fa-arrow-circle-right"/>
            </div>)
}

export default Pagination;
