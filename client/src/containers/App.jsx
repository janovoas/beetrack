import React, { Component } from 'react';
import UsersTable from './UsersTable.jsx';
import ModalAddUser from "../components/ModalAddUser.jsx";
import Button from "../components/Button.jsx";
import SearchInput from "../components/SearchInput.jsx";
import '../styles/css/App.css';

class App extends Component {

	constructor() {
		super();
		this.state = {
			/**
			 * Boolean para mostrar o esconder modal
			 */
			modalVisible: false,
			/**
			 * Query para búsqueda de usuarios
			 */
			query: ''			
		}
	}

	componentDidMount() {
		/**
		 * Awesome Fonts
		 */
		const script = document.createElement("script");
		script.src = "https://use.fontawesome.com/releases/v5.1.0/js/all.js";
		script.async = true;
		document.body.appendChild(script);
	}
  
  	/**
	 * Cambia el estado de la ventana modal, devolviendo el contrario al estado anterior (true o false)
	 * @public
	 */
	toggleModal()  {
		this.setState(prevState => ({
			modalVisible: !prevState.modalVisible,			
		}));
	}

	/**
	 * Búsqueda de usuario al tipear. Busca por nombre o por parte de la descripción.
	 * @param {object} e Event
	 * @public
	 */
	searchUser(e) {
		const val = e.target.value;

		/** Comenzar a buscar al tipear 3 o más caracteres */
		this.setState({
			query: val
		});		
	}

	render() {
		return (
			<div className="App">			
				<header className="dashboard__header">
					<SearchInput
						searchUser={(e) => {this.searchUser(e)}}
						query={this.state.query}
						iconClass="fas fa-search"/>
					<Button
						text="Nuevo Contacto"
						extraClass="text--color-white"
						buttonClick={this.toggleModal.bind(this)} 
						iconClass="fas fa-plus-circle"/>
				</header>			
				<UsersTable
					currentPage={this.state.currentPage}
					query={this.state.query}/>			
				<ModalAddUser
					isVisible={this.state.modalVisible}
					closeModal={this.toggleModal.bind(this)}/>
			</div>
		);
	}
}

export default App;
