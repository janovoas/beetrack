import React, { Component } from 'react';
import UserRow from '../components/UserRow.jsx';
import Pagination from "../components/Pagination";

class UsersTable extends Component {
  constructor(props) {
    super(props);

    this.state = {
			/**
			 * Arreglo de usuarios
			 */
      users: [],
      
			/**
			 * Página actual (para paginación)
			 */
      currentPage: 1,
      
			/**
			 * Query para búsqueda de usuarios
			 */
      query: props.query
    };

		/**
		 * Bind this a función para eliminar usuario
		 */
    this.deleteUser = this.deleteUser.bind(this);
  }

  /**
   * Al montar el componente, obtener usuarios de la página actual
   */
  componentDidMount() {
    this.getUsers(this.state.currentPage);
  }

  /**
   * Obtener usuarios según página actual y query. Si query está vacío, se recuperan todos los usuarios.
   * @param {int} currentPage Página actual de la API. Recuperado desde state.
   * @param {string} query Query a buscar mediante la API. Recuperado desde state.
   */
  getUsers(currentPage = this.state.currentPage, query = this.state.query) {
    fetch(`api/users?_page=${currentPage}&_limit=4&q=${query}`)
      .then(response => {
        return response.json();
      })
      .then(data => {
        if (data.length > 0) {
          this.setState({
            users: data,
            currentPage: currentPage
          });
        }
      })
      .catch(error => console.warn(error));
  }

  /**
   * Eliminar usuario de la base de datos.
   * @param {object} deletedUser Objecto del usuario a eliminar.
   */
  deleteUser(deletedUser) {
    /** DELETE request para eliminar el usuario */
    fetch(`/api/users/${deletedUser.id}`, {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json"
      }
    })
      .then(response => response.json())
      .then(responseJson => {
        console.log(responseJson);
      })
      .catch(error => {
        console.error(error);
      });

    /** Arreglo con filtro de los usuarios anteriores (desde state) sin el usuario eliminado */
    const newUsers = this.state.users.filter(user => {
      return user !== deletedUser;
    });

    /** Actualizar arreglo de usuarios en state */
    this.setState({
      users: newUsers
    });
  }

  /**
   * Cambiar de página.
   * Avanza o retrocede según la clase del botón.
   * Realiza comprobación para no retroceder si la página es igual a 1.
   * Realiza comprobación para no avanzar si el arreglo retornado está vacío.
   * @param {object} e Event
   */
  paginationChange(e) {
    let currentPage = this.state.currentPage;
    let nextPage = 1;
    const toNextPage = e.target.classList.contains("next-page") ? true : false;

    if (toNextPage) {
      nextPage = currentPage + 1;
    } else if (currentPage > 1) {
      nextPage = currentPage - 1;
    }
    this.getUsers(nextPage);
  }

  /**
   * Filtrar Usuarios según búsqueda mediante la actualización de prop.query desde el componente padre.
   * @param {object} nextProps Objeto con las propiedades actualizadas desde el componente padre.
   */
  componentWillReceiveProps(nextProps) {
    if (nextProps.query !== this.state.query) {
      if (nextProps.query.length > 2) {
        this.getUsers(1, nextProps.query);
      } else {
        this.getUsers();
      }
    }
  }

  /**
	 * Render
	 */
  render() {
		/**
		 * Arreglo de objetos de usuarios desde state.
		 */
		const _users = this.state.users;
		
		/**
		 * Placeholder para retornar DOM con listado o con mensaje si está cargando aún.
		 */
    let usersRows;

		/**
		 * Chequear si el arreglo _users contiene elementos.
		 */
    if (_users.length) {
			/**
			 * Obtener mediante map y el componente UserRow las filas con los datos de cada usuario.
			 */
      const rowItems = _users.map(user => (
        <UserRow
          key={user.id.toString()}
          user={user}
          deleteUser={e => this.deleteUser(user)}
        />
			));
			
			/**
			 * Poblar el placeholder con la tabla de usuarios.
			 */
      usersRows = (
        <section className="users__table">
          <table>
            <thead>
              <tr>
                <th>Nombre</th>
                <th>Descripción</th>
              </tr>
            </thead>
            <tbody>{rowItems}</tbody>
          </table>
          <Pagination
            currentPage={this.state.currentPage}
            paginationFunc={e => this.paginationChange(e)}
          />
        </section>
      );
		}
		/**
		 * Feedback "Cargando"
		 */
		else {
      usersRows = <h1 className="text--center">Cargando...</h1>;
		}
		
		/**
		 * Retornar el placeholder ya poblado.
		 */
    return usersRows;
  }
}

export default UsersTable;
