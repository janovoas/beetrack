import React, { Component } from 'react';
import InputField from "../components/InputField.jsx";

class SearchInput extends Component {
	constructor(props) {
		super(props);
		
		this.state = {
			/**
			 * Función para buscar usuario al tipear
			 */
			searchUser: props.searchUser,

			/**
			 * String de búsqueda de usuario
			 */
			query: '',
			
			/**
			 *Ícono del botón (Font Awesome)
			*/
			icon: props.iconClass === "" ? "" : <i className={props.iconClass} />
		}


		/**
		 * Bind this para actualización de value en input
		 */
		this.handleValue = this.handleValue.bind(this);
	}

	/** Handle value */
	handleValue(e) {
		this.setState({ query: e.target.value });
	}
  
	render() {
		return (
			<div className="search__input">
			{this.state.icon}
			<InputField
				id="search-user"
				type="text"
				name="search-user"
				placeholder="Buscar usuario..."
				inputVal={this.state.query}
				controlFunc={this.handleValue}
				onKeyUp={this.state.searchUser}/>
		</div>
		);
	}
}

export default SearchInput;
