import React from 'react';

function InputField(props) {
    if (props.type === 'textarea') {
        return <textarea id={props.id} name={props.name} value={props.inputVal} onChange={props.controlFunc} />;
    }
    else if (props.type === 'select') {
        return <select />;
    }
    else{            
        return <input id={props.id} className="users-list__input" onKeyUp={props.onKeyUp} type={props.type} name={props.name} value={props.inputVal} onChange={props.controlFunc} placeholder={props.placeholder} />;
    }
}

export default InputField;
