import React from 'react';

function FeedbackMsg(props) {
    return <p className={`feedback__message feedback__message-${props.type}`}>{props.msg}</p>
}

export default FeedbackMsg;
