import React from 'react';
import Avatar from './Avatar.jsx';
import Button from './Button.jsx';

function UserRow(props) {
    /**
     * Objeto usuario
     */
    const user = props.user;
    /**
     * Función para eliminar usuario, desde componente UsersTable
     */
    const deleteUser = props.deleteUser;

    return <tr className="user__row" >
            <td className="user__info">
                <Avatar
                user={user} />
                <h4 className="user__name" >{user.name}</h4>
                <Button
                text="Eliminar"
                extraClass="user__delete nobg nopad"
                buttonClick={deleteUser} />
            </td>
            <td className="user__description">
                <p>{user.description}</p>
            </td>
        </tr>;
}

export default UserRow;