import React from 'react';

function Avatar(props) {
    /**
     * Objeto usuario
     */
    const user = props.user;
    return (
        <figure className="user__avatar">
            <img src={user.photo}
            alt={"Avatar de " + user.name}
            title={user.name}
            />
        </figure>
    )
}

export default Avatar;